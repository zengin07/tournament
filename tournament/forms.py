from django import forms
from userprofile.models import UserProfile
from team.models import Team
from .models import MatchPlayers


class TransferOfferForm(forms.Form):

    sender_team = forms.ModelChoiceField(queryset=Team.objects.all())
    receiver = forms.ModelChoiceField(queryset=UserProfile.objects.all())

    def __init__(self, *args, **kwargs):
        for_admin = kwargs.pop('for_admin', None)
        super().__init__(*args, **kwargs)
        self.fields['sender_team'].widget.attrs['class'] = 'select2'
        self.fields['sender_team'].widget.attrs['data-placeholder'] = 'Select sender team'

        self.fields['receiver'].widget.attrs['class'] = 'select2'
        self.fields['receiver'].widget.attrs['data-placeholder'] = 'Select players'

        if not for_admin:
            del self.fields['sender_team']


class MatchPlayersForm(forms.ModelForm):
    player = forms.ModelChoiceField(queryset=UserProfile.objects.all())

    class Meta:
        model = MatchPlayers
        fields = ['player', 'position', 'yellow_card', 'red_card', 'goal', 'mvp']

    def __init__(self, *args, **kwargs):
        match_player_id = kwargs.pop('match_player_id', None)
        # team_id = kwargs.pop('home_id', None)
        # super().__init__(*args, **kwargs)
        # team = Team.objects.get(pk=team_id)

        if match_player_id:
            match_player = MatchPlayers.objects.get(pk=match_player_id)
            # match_player_dict = match_player.__dict__
            kwargs['initial'] = match_player.__dict__
            super().__init__(*args, **kwargs)
            self.fields['player'].initial = match_player.player.pk
        else:
            super().__init__(*args, **kwargs)
