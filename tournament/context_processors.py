from userprofile.models import UserProfile


def add_user_profile(request):
    if request.user.is_authenticated():
        auth_user_profile = UserProfile.objects.get(user_id=request.user.pk)
    else:
        auth_user_profile = UserProfile()
    return {
        'is_logged_in': request.user.is_authenticated(),
        'auth_user_profile': auth_user_profile,
    }
