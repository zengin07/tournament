from django.views import View
from django.shortcuts import render


def index_view(request):
    return render(request, 'tournament/index.html')


def deneme(request):
    return render(request, 'tournament/deneme.html')
