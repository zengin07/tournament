"""tournament URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from userprofile.views import MyRegistrationView
from . import views

urlpatterns = [
    url(r'^$', views.index_view, name='index'),
    url(r'^deneme$', views.deneme, name='deneme'),
    url(r'^admin/', admin.site.urls),
    url(r'^user/', include('userprofile.urls')),
    url(r'^match/', include('match.urls')),
    url(r'^tourney/', include('tourney.urls')),
    url(r'^accounts/register/$', MyRegistrationView.as_view(), name='registration_register'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^', include('registration.auth_urls')),
    # url(r'^', include('django.contrib.auth.urls')),
    url(r'^team/', include('team.urls')),
]
