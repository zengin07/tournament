from django.db import models
from django.utils import timezone

from match.models import Match
from tourney.models import League, Knockout
from userprofile.models import UserProfile
from team.models import Team
from userprofile.choices import *

# Create your models here.


class TeamCaptains(models.Model):
    team = models.ForeignKey(Team)
    player = models.ForeignKey(UserProfile, null=True)


class TransferOffer(models.Model):
    sender_team = models.ForeignKey(Team)
    receiver = models.ForeignKey(UserProfile)
    is_accepted = models.BooleanField(default=False)
    is_answered = models.BooleanField(default=False)
    date = models.DateTimeField(default=timezone.now)

    # class Meta:
    #     unique_together = ('sender_team', 'receiver',)


class PlayerTransfers(models.Model):
    player = models.ForeignKey(UserProfile)
    team = models.ForeignKey(Team)
    transfer_date = models.DateTimeField(default=timezone.now)


class TourneyMatches(models.Model):
    league = models.ForeignKey(League, null=True)
    knockout = models.ForeignKey(Knockout, null=True)
    match = models.OneToOneField(Match)
    week = models.IntegerField()
    group = models.IntegerField(null=True)
    type = models.IntegerField()  # 1 ==> league, 2 ==> knockout


class MatchPlayers(models.Model):
    HOME = 1
    AWAY = 2
    TEAM = (
        (HOME, "Home"),
        (AWAY, "Away")
    )
    player = models.ForeignKey(UserProfile)
    match = models.ForeignKey(Match)
    position = models.IntegerField(choices=POSITION, default=0)
    yellow_card = models.IntegerField(default=0)
    red_card = models.IntegerField(default=0)
    goal = models.IntegerField(default=0)
    mvp = models.BooleanField(default=False)
    team = models.CharField(max_length=1, choices=TEAM)
