from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<match_id>[0-9]+)$', views.MatchPage.as_view(), name='match_page'),
    url(r'^edit/(?P<match_id>[0-9]+)$', views.EditMatch.as_view(), name='edit_match'),
]
