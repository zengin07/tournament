import base64
import json

from django.db.models.query_utils import Q
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.views import View

from match.models import Match
from tournament.models import TeamCaptains, TourneyMatches, MatchPlayers
from tournament.forms import MatchPlayersForm
from tourney.models import League
from userprofile.models import UserProfile
from . import forms
from . import models
# Create your views here.


class MatchPage(View):
    def get(self, request, match_id):
        match = Match.objects.get(pk=match_id)
        if match.squad_home:
            match.squad_home = [UserProfile.objects.get(user_id=int(pid)) for pid in match.squad_home.split(',')]
        if match.squad_away:
            match.squad_away = [UserProfile.objects.get(user_id=int(pid)) for pid in match.squad_away.split(',')]
        return render(request, 'match/match_page.html', {'match': match})


class EditMatch(View):
    def get(self, request, match_id, info_message=''):
        if not request.GET.get('for_team'):
            request.GET = request.GET.copy()
            request.GET['for_team'] = 'home'

        cup = UserProfile.objects.get(user_id=request.user.pk)
        match = models.Match.objects.get(pk=match_id)

        captain_home = TeamCaptains.objects.get(team_id=match.home.pk).player_id
        captain_away = TeamCaptains.objects.get(team_id=match.away.pk).player_id

        if not cup.user_type == 1 and not (request.user.pk == captain_home or request.user.pk == captain_away):
            return redirect('index')

        initial = match.__dict__
        if match.squad_home:
            squad_home_list = match.squad_home.split(',')
            initial['squad_home'] = squad_home_list
        if match.squad_home:
            squad_away_list = match.squad_away.split(',')
            initial['squad_away'] = squad_away_list
        form = forms.MatchForm(home_id=match.home.pk, away_id=match.away.pk, initial=initial)

        if not (request.user.pk == captain_home or request.user.pk == captain_away):
            del form.fields["ss1"]
            del form.fields["ss2"]
            del form.fields["ss3"]
            del form.fields["ss4"]
            del form.fields["ss5"]

        screenshots = []
        if cup.team == match.home:
            screenshots.append(match.ss1_home) if match.ss1_home else screenshots.append('')
            screenshots.append(match.ss2_home) if match.ss2_home else screenshots.append('')
            screenshots.append(match.ss3_home) if match.ss3_home else screenshots.append('')
            screenshots.append(match.ss4_home) if match.ss4_home else screenshots.append('')
            screenshots.append(match.ss5_home) if match.ss5_home else screenshots.append('')
        elif cup.team == match.home:
            screenshots.append(match.ss1_away) if match.ss1_away else screenshots.append('')
            screenshots.append(match.ss2_away) if match.ss2_away else screenshots.append('')
            screenshots.append(match.ss3_away) if match.ss3_away else screenshots.append('')
            screenshots.append(match.ss4_away) if match.ss4_away else screenshots.append('')
            screenshots.append(match.ss5_away) if match.ss5_away else screenshots.append('')

        if cup.pk == captain_home:
            match_players = MatchPlayers.objects.filter(Q(match=match) & Q(team=1))
        elif cup.pk == captain_away:
            match_players = MatchPlayers.objects.filter(Q(match=match) & Q(team=2))
        elif cup.user_type == 1:
            if request.GET['for_team'] == 'home':
                match_players = MatchPlayers.objects.filter(Q(match=match) & Q(team=1))
            elif request.GET['for_team'] == 'away':
                match_players = MatchPlayers.objects.filter(Q(match=match) & Q(team=2))

        match_player_forms = []
        for match_player in match_players:
            match_player_forms.append(MatchPlayersForm(match_player_id=match_player.pk))

        context = {
            'form': form,
            'screenshots': screenshots,
            'match_player_form': MatchPlayersForm(),
            'match_player_forms': match_player_forms,
            'match': match,
        }
        if info_message:
            context['info_message'] = info_message
        return render(request, 'match/edit_match.html', context)

    def post(self, request, match_id):
         if not request.GET.get('for_team'):
            request.GET = request.GET.copy()
            request.GET['for_team'] = 'home'

        cup = UserProfile.objects.get(user_id=request.user.pk)
        match = models.Match.objects.get(pk=match_id)
        request.POST = request.POST.copy()

        if match.played:
            return self.get(request, match_id, 'This match can not be edited since it is marked as played by Admin')

        request.POST['squad_home'] = ','.join(request.POST.getlist('squad_home'))
        request.POST['squad_away'] = ','.join(request.POST.getlist('squad_away'))

        captain_home = TeamCaptains.objects.get(team_id=match.home.pk).player_id
        captain_away = TeamCaptains.objects.get(team_id=match.away.pk).player_id
        if not (request.user.pk == captain_home or request.user.pk == captain_away) and not cup.user_type == 1:
            return redirect('index')

        form = forms.MatchForm(request.POST, request.FILES, home_id=match.home.pk, away_id=match.away.pk)

        form.is_valid()
        ss1 = "data:image/jpg;base64," + base64.b64encode(form.cleaned_data['ss1'].read()).decode('ascii') if form.cleaned_data['ss1'] else ''
        ss2 = "data:image/jpg;base64," + base64.b64encode(form.cleaned_data['ss2'].read()).decode('ascii') if form.cleaned_data['ss2'] else ''
        ss3 = "data:image/jpg;base64," + base64.b64encode(form.cleaned_data['ss3'].read()).decode('ascii') if form.cleaned_data['ss3'] else ''
        ss4 = "data:image/jpg;base64," + base64.b64encode(form.cleaned_data['ss4'].read()).decode('ascii') if form.cleaned_data['ss4'] else ''
        ss5 = "data:image/jpg;base64," + base64.b64encode(form.cleaned_data['ss5'].read()).decode('ascii') if form.cleaned_data['ss5'] else ''

        match.squad_home = form.cleaned_data['squad_home']
        match.squad_away = form.cleaned_data['squad_away']

        match.result_home = form.cleaned_data['result_home']
        match.result_away = form.cleaned_data['result_away']

        if cup.team == match.home:
            match.ss1_home = ss1 if ss1 else match.ss1_home
            match.ss2_home = ss2 if ss2 else match.ss2_home
            match.ss3_home = ss3 if ss3 else match.ss3_home
            match.ss4_home = ss4 if ss4 else match.ss4_home
            match.ss5_home = ss5 if ss5 else match.ss5_home
        elif cup.team == match.away:
            match.ss1_away = ss1 if ss1 else match.ss1_away
            match.ss2_away = ss2 if ss2 else match.ss2_away
            match.ss3_away = ss3 if ss3 else match.ss3_away
            match.ss4_away = ss4 if ss4 else match.ss4_away
            match.ss5_away = ss5 if ss5 else match.ss5_away

        if cup.user_type == 1:
            match.played = form.cleaned_data["played"]

        match_player_strings = request.POST['match_players'].split(',')

        # creating MatchPlayers entries fo database
        player_dicts = []
        for string in match_player_strings:
            if string == '':
                continue
            elif string[0] == '&':
                string = string[1:]
            player_dict = {}
            splitted = string.split('&')
            for kv in splitted:
                if kv == '':
                    continue
                key, value = kv.split('=')
                player_dict[key] = value

            player_dicts.append(player_dict)

        if cup.pk == captain_home:
            match_players = MatchPlayers.objects.filter(Q(match=match) & Q(team=1))
        elif cup.pk == captain_away:
            match_players = MatchPlayers.objects.filter(Q(match=match) & Q(team=2))
        elif cup.user_type == 1:
            if request.GET['for_team'] == 'home':
                match_players = MatchPlayers.objects.filter(Q(match=match) & Q(team=1))
            elif request.GET['for_team'] == 'away':
                match_players = MatchPlayers.objects.filter(Q(match=match) & Q(team=2))

        match_players = list(match_players)
        while len(match_players) != len(player_dicts):
            if len(match_players) < len(player_dicts):
                match_players.append(MatchPlayers())
            else:
                match_players[0].delete()
                match_players = match_players[1:]

        for i, player_dict in enumerate(player_dicts):
            mpform = MatchPlayersForm(player_dict)
            if mpform.is_valid():
                target_match_player = match_players[i]
                target_match_player.match = match
                target_match_player.player = mpform.cleaned_data['player']
                target_match_player.yellow_card = mpform.cleaned_data['yellow_card']
                target_match_player.red_card = mpform.cleaned_data['red_card']
                target_match_player.position = mpform.cleaned_data['position']
                target_match_player.goal = mpform.cleaned_data['goal']
                target_match_player.mvp = mpform.cleaned_data['mvp']
                if cup.pk == captain_home:
                    target_match_player.team = target_match_player.HOME
                elif cup.pk == captain_home:
                    target_match_player.team = target_match_player.AWAY
                elif cup.user_type == 1:
                    if request.GET['for_team'] == 'home':
                        target_match_player.team = target_match_player.HOME
                    elif request.GET['for_team'] == 'away':
                        target_match_player.team = target_match_player.AWAY
                target_match_player.save()
            else:
                return self.get(request, match_id, 'Invalid form data')

        match.save()
        return redirect('match_page', match_id=match_id)
