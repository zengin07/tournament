from django import forms
from django.forms import ModelForm

from team.models import Team
from .models import Match
from userprofile.models import UserProfile


class MatchForm(ModelForm):
    squad_home = forms.ModelMultipleChoiceField(queryset=None)
    squad_away = forms.ModelMultipleChoiceField(queryset=None)
    ss1 = forms.FileField(required=False)
    ss2 = forms.FileField(required=False)
    ss3 = forms.FileField(required=False)
    ss4 = forms.FileField(required=False)
    ss5 = forms.FileField(required=False)

    class Meta:
        model = Match
        fields = ['squad_home', 'squad_away', 'result_home', 'result_away', 'played']

    def __init__(self, *args, **kwargs):
        home_id = kwargs.pop('home_id', None)
        away_id = kwargs.pop('away_id', None)
        super().__init__(*args, **kwargs)
        team1 = Team.objects.get(pk=home_id)
        team2 = Team.objects.get(pk=away_id)

        # self.fields["result_home"].initial = None
        # self.fields["result_away"].initial = None

        self.fields["squad_home"].label = "Squad " + team1.name
        self.fields["squad_away"].label = "Squad " + team2.name

        self.fields["result_home"].label = "Result " + team1.name
        self.fields["result_away"].label = "Result " + team2.name

        self.fields["squad_home"].queryset = UserProfile.objects.filter(team_id=home_id)
        self.fields["squad_away"].queryset = UserProfile.objects.filter(team_id=away_id)

        self.fields['squad_home'].widget.attrs['class'] = 'select2'
        self.fields['squad_home'].widget.attrs['data-placeholder'] = 'Select players'

        self.fields['squad_away'].widget.attrs['class'] = 'select2'
        self.fields['squad_away'].widget.attrs['data-placeholder'] = 'Select players'

    def clean(self):
        super().clean()
        self.cleaned_data['squad_home'] = self.data['squad_home']
        self.cleaned_data['squad_away'] = self.data['squad_away']
        return self.cleaned_data
