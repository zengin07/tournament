from django.core.validators import validate_comma_separated_integer_list
from django.db import models
from team.models import Team
from tournament import models as tm_models
from tourney import models as tourneymodels


class Match(models.Model):
    HOME_WIN = 1
    DRAW = 2
    AWAY_WIN = 3

    home = models.ForeignKey(Team, related_name='Home')
    away = models.ForeignKey(Team, related_name='Away')
    squad_home = models.CharField(max_length=100, validators=[validate_comma_separated_integer_list], null=True)
    squad_away = models.CharField(max_length=100, validators=[validate_comma_separated_integer_list], null=True)
    result_home = models.IntegerField(null=True)
    result_away = models.IntegerField(null=True)
    date = models.DateTimeField(null=True)
    played = models.BooleanField(default=False)
    return_match = models.OneToOneField('Match', null=True, related_name="returnmatch")
    extra_match = models.OneToOneField('Match', null=True, related_name="extramatch")

    ss1_home = models.TextField(null=True)
    ss2_home = models.TextField(null=True)
    ss3_home = models.TextField(null=True)
    ss4_home = models.TextField(null=True)
    ss5_home = models.TextField(null=True)

    ss1_away = models.TextField(null=True)
    ss2_away = models.TextField(null=True)
    ss3_away = models.TextField(null=True)
    ss4_away = models.TextField(null=True)
    ss5_away = models.TextField(null=True)

    league_id = None
    knockout_id = None
    week = None
    group = None
    type = None  # 1 ==> league , 2==> knockout

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        is_update = 0 if self.pk is None else 1

        if update_fields:
            super().save()
            return
        if self.home == self.away:
            self.played = True

        super().save()
        if is_update:
            # self.played = True
            super().save()  # updating match is done update related things below

            tourney_match = tm_models.TourneyMatches.objects.get(match_id=self.pk)
            if tourney_match.league is not None:
                league = tourneymodels.League.objects.get(pk=tourney_match.league_id)
                if league.knockout is not None:
                    if not league.knockout.knockout_stage:  # if knockout stage did not started
                        league.knockout.check_groups()
            elif tourney_match.knockout is not None:
                knockout = tourneymodels.Knockout.objects.get(pk=tourney_match.knockout_id)
                if not knockout.final_created:
                    knockout.check_knockout_tours()  # check if knockout tour matches finished and create new ones if needed
                elif knockout.final_created and self.played:
                    knockout.set_finished(self.get_winner())
        else:
            new_tm = tm_models.TourneyMatches()
            if self.league_id:
                new_tm.league_id = self.league_id
            if self.knockout_id:
                new_tm.knockout_id = self.knockout_id
            new_tm.match = self
            new_tm.week = self.week
            new_tm.group = self.group
            new_tm.type = self.type
            new_tm.save()

    def delete(self, using=None, keep_parents=False):
        super().delete()
        tm = tm_models.TourneyMatches.objects.get(match=self)
        tm.delete()

    def get_result(self):
        # this returns 1/2/3
        # 1 => home win
        # 2 => draw
        # 3 => away win

        if self.is_self_played():
            return self.HOME_WIN
        elif self.result_home > self.result_away:
            return self.HOME_WIN
        elif self.result_home == self.result_away:
            return self.DRAW
        elif self.result_home < self.result_away:
            return self.AWAY_WIN

    def get_goal_difference_winner(self):
        team1_goal_count = self.result_home + self.return_match.result_away  # team1 is gonna be self.home and self.return_match.away
        team2_goal_count = self.result_away + self.return_match.result_home  # team2 is gonna be self.away and self.return_match.home

        if team1_goal_count > team2_goal_count:
            return self.home
        elif team1_goal_count < team2_goal_count:
            return self.away
        else:
            if self.result_away > self.return_match.result_away:  # means team2 has more goals away
                return self.away
            elif self.result_away < self.return_match.result_away:  # means team1 has more goals away
                return self.home
            else:
                if self.return_match.extra_match is not None:
                    additional_match = self.return_match.extra_match
                    return additional_match.get_winner()
                else:
                    return False

    def get_winner(self):
        if self.played:
            if self.is_self_played():
                return self.home

            if self.get_result() == self.HOME_WIN:
                return self.home
            elif self.get_result() == self.AWAY_WIN:
                return self.away
        else:
            return None

    def is_self_played(self):
        return self.home == self.away

    def super_save(self):
        super().save()
