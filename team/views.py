from django.views import View
from django.db.models import Q
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse

from match.models import Match
from tournament.models import TeamCaptains, TourneyMatches
from tourney.models import League, Knockout
from userprofile.models import UserProfile
from userprofile.choices import *
from . import forms
from . import models


# Create your views here.


class CreateTeam(LoginRequiredMixin, View):
    def get(self, request):
        user_profile = UserProfile.objects.get(user_id=request.user.pk)
        if user_profile.user_type != ADMIN:
            return render('tournament/index.html', {'info_message': "Access Denied!"})

        form = forms.TeamForm()
        players = UserProfile.objects.all()
        context = {'form': form, 'players': players, 'user_profile': user_profile}
        return render(request, "team/create_team.html", context)

    def post(self, request):
        user_profile = UserProfile.objects.get(user_id=request.user.pk)
        if user_profile.user_type != ADMIN:
            return render('tournament/index.html', {'info_message': "Access Denied!"})

        form = forms.TeamForm(request.POST)
        players = UserProfile.objects.all()
        if form.is_valid():
            new_team = models.Team()
            new_team.name = form.cleaned_data['name']
            new_team.cash = form.cleaned_data['cash']
            new_team.save()

            players = form.data.getlist("players")
            captain = form.cleaned_data['captain']
            for player_id in players:
                player = UserProfile.objects.get(user_id=player_id)
                player.team = new_team
                if player.user_type == CAPTAIN:
                    player.user_type = PLAYER
                if player.pk == captain.pk:
                    if player.user_type == PLAYER:
                        player.user_type = CAPTAIN

                    tc = TeamCaptains(team_id=new_team.pk)
                    tc.player = player
                    tc.save()

                player.save()
        else:
            context = {'form': form, 'players': players, 'user_profile': user_profile}
            return render(request, "team/create_team.html", context)

        return redirect('edit_team', team_id=new_team.pk)


class EditTeam(LoginRequiredMixin, View):
    def get(self, request, team_id):
        user_profile = UserProfile.objects.get(user_id=request.user.pk)
        if user_profile.user_type != ADMIN and user_profile.user_type != CAPTAIN:
            return render('tournament/index.html', {'info_message': "Access Denied!"})

        if team_id == 0 and user_profile.user_type == CAPTAIN:
            return redirect('edit_team_id', team_id=request.team_id)

        team = models.Team.objects.get(pk=team_id)
        team_players = UserProfile.objects.filter(team_id=team_id)
        team.players = [x.pk for x in team_players]

        try:
            team.captain = TeamCaptains.objects.get(team_id=team_id).player_id
        except TeamCaptains.DoesNotExist:
            team.captain = None
        form = forms.EditTeamForm(initial=team.__dict__, team_id=team_id)

        if user_profile.user_type == CAPTAIN and user_profile.user.id != team.captain:
            return render('tournament/index.html', {'info_message': "This is not your team!"})

        context = {'form': form, 'user_profile': user_profile}
        return render(request, "team/create_team.html", context)

    def post(self, request, team_id):
        user_profile = UserProfile.objects.get(user_id=request.user.pk)
        if user_profile.user_type != ADMIN and user_profile.user_type != CAPTAIN:
            return render('tournament/index.html', {'info_message': "Access Denied!"})

        form = forms.EditTeamForm(request.POST, team_id=team_id)

        if user_profile.user_type == CAPTAIN:
            del form.fields['cash']

        if form.is_valid():
            team = models.Team.objects.get(pk=team_id)
            team.name = form.cleaned_data['name']
            if user_profile.user_type == ADMIN:
                team.cash = form.cleaned_data['cash']
            team.save()

            team_players = UserProfile.objects.filter(team_id=team_id)
            for player in team_players:
                player.team = None
                player.save()

            players = form.data.getlist("players")
            captain = form.cleaned_data['captain']
            for player_id in players:
                player = UserProfile.objects.get(user_id=player_id)
                player.team = team
                if player.user_type == CAPTAIN:
                    player.user_type = PLAYER
                if player.pk == captain.pk:
                    if player.user_type == PLAYER:
                        player.user_type = CAPTAIN

                    tc = TeamCaptains.objects.get_or_create(team_id=team_id)[0]
                    if tc.player_id:
                        old_captain = UserProfile.objects.get(user_id=tc.player_id)
                        if old_captain.user_type == CAPTAIN:
                            old_captain.user_type = PLAYER
                            old_captain.save()

                    tc.player = player
                    tc.save()

                player.save()
        else:
            print(form.errors)
            context = {'form': form, 'user_profile': user_profile}
            return render(request, "team/create_team.html", context)

        context = {'form': form, 'user_profile': user_profile}
        return render(request, "team/create_team.html", context)


class ListTeam(LoginRequiredMixin, View):
    def get(self, request):
        teams = models.Team.objects.all()
        for team in teams:
            team.players = UserProfile.objects.filter(team=team)

        context = {'teams': teams}

        user_profile = UserProfile.objects.get(user_id=request.user.pk)
        if user_profile.user_type == CAPTAIN:
            if user_profile.team_id:
                return redirect("edit_team", team_id=user_profile.team_id)
            else:
                return render('tournament/index.html', {'info_message': "You are not a team member!"})

        return render(request, 'team/list_team.html', context)


class TeamPage(View):
    def get(self, request, team_id):
        team = models.Team.objects.get(pk=team_id)
        players = UserProfile.objects.filter(team_id=team_id)
        matches = Match.objects.filter(Q(home_id=team_id) | Q(away_id=team_id))
        matches = [match for match in matches if not match.is_self_played()]

        league_dict = {}
        knockout_dict = {}
        tourney_matches = [TourneyMatches.objects.get(match_id=match.pk) for match in matches]
        for tm in tourney_matches:
            if tm.knockout:
                if tm.knockout.pk not in knockout_dict:
                    tm.knockout.leagues = []
                    tm.knockout.matches = []
                    knockout_dict[tm.knockout.pk] = tm.knockout
                knockout_dict[tm.knockout.pk].matches.append(tm.match)
            elif tm.league:
                if tm.league.pk not in league_dict:
                    tm.league.matches = []
                    league_dict[tm.league.pk] = tm.league
                league_dict[tm.league.pk].matches.append(tm.match)

        # sorted(scoreboard.items(), key=lambda x: x[1]['pts'], reverse=True)
        knockout_leagues = [league for pk, league in league_dict.items() if league.knockout]
        for league in knockout_leagues:
            if league.knockout.pk not in knockout_dict:
                league.knockout.matches = []
                league.knockout.leagues = []
                knockout_dict[league.knockout.pk] = league.knockout
            knockout_dict[league.knockout.pk].leagues.append(league)

        leagues = list([league for league in league_dict.values() if not league.knockout])  # League.objects.filter(knockout_id__isnull=True)
        knockouts = list(knockout_dict.values())  # Knockout.objects.filter()

        # for league in leagues:
        #     league.matches = []
        #     for tm in TourneyMatches.objects.filter(league=league):
        #         if tm.match.home == team or tm.match.away == team:
        #             if not tm.match.is_self_played():
        #                 league.matches.append(tm.match)
        #
        # for knockout in knockouts:
        #     knockout_leagues = knockout.group_leagues.split(',')
        #     knockout.leagues = [League.objects.get(pk=i) for i in knockout_leagues]
        #     knockout.matches = [tm.match for tm in TourneyMatches.objects.filter(knockout_id=knockout.pk)]
        #     knockout.matches = [match for match in knockout.matches if not match.is_self_played()]
        #     for league in knockout.leagues:
        #         league.matches = []
        #         for tm in TourneyMatches.objects.filter(league=league):
        #             if tm.match.home == team or tm.match.away == team:
        #                 if not tm.match.is_self_played():
        #                     league.matches.append(tm.match)
        #
        # leagues = [league for league in leagues if len(league.matches) != 0]
        #
        # for knockout in knockouts:
        #     knockout.leagues = [league for league in knockout.leagues if len(league.matches) != 0]
        #
        # knockouts = [knockout for knockout in knockouts if len(knockout.leagues) != 0]

        return render(request, "team/team_page.html", {'team': team, 'players': players, 'leagues': leagues, 'knockouts': knockouts})


class SearchTeam(View):
    def get(self, request):
        q = request.GET['q']
        results = models.Team.objects.filter(Q(name__icontains=q))
        for team in results:
            team.players = UserProfile.objects.filter(team=team)

        context = {'teams': results}

        return render(request, 'team/list_team.html', context)
