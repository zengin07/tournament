from django import forms
from . import models
from userprofile.models import UserProfile


class MyModelChoiceField(forms.ModelChoiceField):
    def to_python(self, value):
        return UserProfile.objects.get(pk=value)


class TeamForm(forms.ModelForm):

    players = forms.ModelMultipleChoiceField(queryset=UserProfile.objects.all())
    captain = forms.ModelChoiceField(queryset=UserProfile.objects.all(), empty_label=None, widget=forms.Select(choices=[]))
    # captain = forms.ChoiceField()

    class Meta:
        model = models.Team
        fields = ['name', 'cash']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['players'].widget.attrs['class'] = 'select2'
        self.fields['players'].widget.attrs['data-placeholder'] = 'Select players'

        self.fields['captain'].widget.attrs['class'] = 'select2'
        self.fields['captain'].widget.attrs['data-placeholder'] = 'Select Captain'


class EditTeamForm(TeamForm):
    captain = MyModelChoiceField(required=False, queryset=None, empty_label=None)

    def __init__(self, *args, **kwargs):
        team_id = kwargs.pop('team_id', None)
        super().__init__(*args, **kwargs)
        self.fields['captain'].widget.attrs['class'] = 'select2'
        self.fields['captain'].widget.attrs['data-placeholder'] = 'Select Captain'
        if team_id:
            self.fields['captain'].queryset = UserProfile.objects.filter(team_id=team_id)
