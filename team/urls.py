from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<team_id>[0-9]+)$', views.TeamPage.as_view(), name='team_page'),
    url(r'^create', views.CreateTeam.as_view(), name='create_team'),
    url(r'^edit$', views.ListTeam.as_view(), name='list_team'),
    url(r'^edit/(?P<team_id>[0-9]+)$', views.EditTeam.as_view(), name='edit_team'),
    url(r'^search$', views.SearchTeam.as_view(), name='search_team'),

]
