from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.views import View

from tournament.models import TourneyMatches
from userprofile.models import UserProfile
from .forms import *
from .models import *
from match.models import Match
# Create your views here.


class CreateLeague(LoginRequiredMixin, View):
    def get(self, request):
        cup = UserProfile.objects.get(user_id=request.user.pk)

        form = LeagueForm(league_id=0)
        return render(request, 'tourney/edit_tourney.html', {'form': form})

    def post(self, request):
        cup = UserProfile.objects.get(user_id=request.user.pk)
        form = LeagueForm(request.POST, league_id=0)

        form.is_valid()
        league = League()
        teams = request.POST.getlist('teams')

        league.team_count = len(teams)
        league.teams = ','.join(teams)
        league.name = form.cleaned_data["name"]
        league.save()

        return redirect('edit_league', league_id=league.pk)


class EditLeague(LoginRequiredMixin, View):
    def get(self, request, league_id):
        form = LeagueForm(league_id=league_id)
        league = League.objects.get(pk=league_id)
        fixture = eval(league.fixture)
        for wi, week in enumerate(fixture):
            for mi, match_id in enumerate(week):
                fixture[wi][mi] = Match.objects.get(pk=match_id)

        return render(request, 'tourney/edit_tourney.html', {'form': form, 'fixture': fixture})

    def post(self, request, league_id):
        cup = UserProfile.objects.get(user_id=request.user.pk)
        form = LeagueForm(request.POST, league_id=0)

        league = League.objects.get(pk=league_id)
        fixture = eval(league.fixture)
        for wi, week in enumerate(fixture):
            for mi, match_id in enumerate(week):
                fixture[wi][mi] = Match.objects.get(pk=match_id)

        form.is_valid()
        teams = request.POST.getlist('teams')

        if league.team_count == len(teams):
            league.teams = ','.join(teams)
            league.name = form.cleaned_data["name"]
            league.save()
        else:
            return render(request, 'tourney/edit_tourney.html', {'form': LeagueForm(league_id=league_id), 'fixture': fixture, 'info_message': 'Team count can not be changed'})

        return redirect('edit_league', league_id=league_id)


class CreateKnockout(LoginRequiredMixin, View):
    def get(self, request):
        cup = UserProfile.objects.get(user_id=request.user.pk)

        form = KnockoutForm(knockout_id=0)
        return render(request, 'tourney/edit_tourney.html', {'form': form})

    def post(self, request):
        cup = UserProfile.objects.get(user_id=request.user.pk)
        form = KnockoutForm(request.POST, knockout_id=0)

        form.is_valid()
        knockout = Knockout()
        teams = request.POST.getlist('teams')

        knockout.team_count = len(teams)
        knockout.teams = ','.join(teams)
        knockout.group_count = form.cleaned_data['group_count']
        knockout.winner_count = form.cleaned_data['winner_count']
        knockout.return_match = form.cleaned_data['return_match']
        knockout.name = form.cleaned_data["name"]
        knockout.save()

        return redirect('edit_knockout', knockout_id=knockout.pk)


class EditKnockout(LoginRequiredMixin, View):
    def get(self, request, knockout_id):
        cup = UserProfile.objects.get(user_id=request.user.pk)

        form = KnockoutForm(knockout_id=knockout_id)
        return render(request, 'tourney/edit_tourney.html', {'form': form})

    def post(self, request, knockout_id):
        cup = UserProfile.objects.get(user_id=request.user.pk)
        form = KnockoutForm(request.POST, knockout_id=knockout_id)

        form.is_valid()
        knockout = Knockout.objects.get(pk=knockout_id)
        teams = request.POST.getlist('teams')

        knockout.teams = ','.join(teams)
        # knockout.return_match = form.cleaned_data['return_match']
        knockout.name = form.cleaned_data["name"]
        knockout.save()

        # return redirect('edit_kncok', league_id=league.pk)
        return redirect('edit_knockout', knockout_id=knockout_id)


class ListTourneys(View):
    def get(self, request):
        leagues = League.objects.filter(knockout_id__isnull=True)
        knockouts = Knockout.objects.all()

        return render(request, 'tourney/list_tourney.html', {'leagues': leagues, 'knockouts': knockouts})


class LeaguePage(View):
    def get(self, request, league_id):
        league = League.objects.get(pk=league_id)
        league.scoreboard = [(Team.objects.get(pk=team_id),point) for team_id, point in league.get_scoreboard()]
        league.fixture = [[Match.objects.get(pk=int(m)) for m in w] for w in eval(league.fixture)]

        return render(request, 'tourney/league_page.html', {'league': league})


class KnockoutPage(View):
    def get(self, request, knockout_id):
        knockout = Knockout.objects.get(pk=knockout_id)
        knockout.groups = [League.objects.get(pk=int(league_id)) for league_id in knockout.group_leagues.split(',')]
        knockout.matches = [tm.match for tm in TourneyMatches.objects.filter(knockout=knockout)]

        return render(request, 'tourney/knockout_page.html', {'knockout': knockout})


  # league = League.objects.get(pk=league_id)
  #       scoreboard = league.get_scoreboard()
  #       league.scoreboard = []
  #       for row in scoreboard:
  #           league.scoreboard.append((Team.objects.get(pk=row[0]), row[1]))
  #       return render(request, 'tourney/league_page.html', {'league': league})
