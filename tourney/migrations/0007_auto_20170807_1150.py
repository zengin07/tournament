# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-07 11:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tourney', '0006_league_scoreboard'),
    ]

    operations = [
        migrations.AlterField(
            model_name='league',
            name='scoreboard',
            field=models.TextField(default='{}'),
        ),
    ]
