from django import forms
from django.forms import ModelForm
from .models import *
from team.models import Team


class LeagueForm(ModelForm):
    teams = forms.ModelMultipleChoiceField(queryset=Team.objects.all())

    class Meta:
        model = League
        fields = ['name', 'teams']

    def __init__(self, *args, **kwargs):
        league_id = kwargs.pop('league_id', None)
        super().__init__(*args, **kwargs)
        if league_id:
            league = League.objects.get(pk=league_id)
            league.teams = league.teams.split(',')
            self.fields["teams"].initial = Team.objects.filter(id__in=league.teams)
            self.fields["name"].initial = league.name

        self.fields['teams'].widget.attrs['class'] = 'select2'
        self.fields['teams'].widget.attrs['data-placeholder'] = 'Select teams'


class KnockoutForm(ModelForm):
    teams = forms.ModelMultipleChoiceField(queryset=Team.objects.all())

    class Meta:
        model = Knockout
        fields = ['name', 'teams', 'group_count', 'winner_count', 'return_match']

    def __init__(self, *args, **kwargs):
        knockout_id = kwargs.pop('knockout_id', None)
        super().__init__(*args, **kwargs)
        if knockout_id:
            knockout = Knockout.objects.get(pk=knockout_id)
            knockout.teams = knockout.teams.split(',')
            knockout.group_leagues = knockout.group_leagues.split(',')
            self.fields["teams"].initial = Team.objects.filter(id__in=knockout.teams)
            self.fields["name"].initial = knockout.name
            del self.fields["return_match"]
            del self.fields['group_count']
            del self.fields['winner_count']

        self.fields['teams'].widget.attrs['class'] = 'select2'
        self.fields['teams'].widget.attrs['data-placeholder'] = 'Select teams'
