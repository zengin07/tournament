import json

from django.core.validators import validate_comma_separated_integer_list
from django.db import models
from django.db.models.query_utils import Q

from match import models as matchmodels
from team.models import Team
from random import shuffle
import operator
import math

from tournament import models as tm_models


class Knockout(models.Model):
    name = models.CharField(max_length=100)
    teams = models.CharField(validators=[validate_comma_separated_integer_list], max_length=400, null=True)
    team_count = models.IntegerField()
    group_count = models.IntegerField()
    group_leagues = models.CharField(max_length=200, validators=[validate_comma_separated_integer_list], null=True)
    winner_count = models.IntegerField()
    fixture = models.CharField(max_length=1000, null=True)
    return_match = models.BooleanField(default=False)
    tour = models.IntegerField(default=0)
    knockout_stage = models.BooleanField(default=False)
    final_created = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)
    winner = models.OneToOneField(Team, null=True)

    # winners = models.CharField(validators=[validate_comma_separated_integer_list], max_length=400, null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        is_update = 0 if self.pk is None else 1
        super().save()  # to create pk
        if is_update:
            self.update_groups()
        else:
            self.create_groups()
        super().save()  # to save groups

    def update_groups(self):
        knockout_teams = self.teams.split(',')
        shuffle(knockout_teams)

        group_leagues = League.objects.filter(knockout_id=self.pk)
        team_per_group = self.team_count / self.group_count
        team_per_group = math.ceil(team_per_group)
        group_teams = [knockout_teams[x:x+team_per_group] for x in range(0, len(knockout_teams), team_per_group)]

        for i, teams in enumerate(group_teams):
            league = group_leagues[i]
            league.teams = ','.join(teams)
            league.save()

    def create_groups(self):
        knockout_teams = self.teams.split(',')
        shuffle(knockout_teams)

        team_per_group = self.team_count / self.group_count
        team_per_group = math.ceil(team_per_group)
        group_teams = [knockout_teams[x:x+team_per_group] for x in range(0, len(knockout_teams), team_per_group)]

        self.group_leagues = []
        for i, teams in enumerate(group_teams):
            league = League()

            league.name = self.name + "- Group" + chr(i+65)
            league.team_count = len(teams)
            league.teams = ','.join(teams)
            league.knockout = self
            league.save()
            self.group_leagues.append(str(league.pk))

        self.group_leagues = ','.join(self.group_leagues)

    def check_groups(self):
        group_leagues = self.group_leagues.split(',')
        winners = []

        for league_id in group_leagues:
            league = League.objects.get(pk=league_id)
            league_fixture = eval(league.fixture)
            for week in league_fixture:
                for match_id in week:
                    match = matchmodels.Match.objects.get(pk=match_id)
                    if match.played:
                        continue
                    else:
                        return False

        # if all matches are played group stage is finished
        # then create knockout stage
        for league_id in group_leagues:
            league_winners = []
            league = League.objects.get(pk=league_id)
            scoreboard = league.get_scoreboard()  # score board returns as [(team_id, point)]
            for team_id, point in scoreboard[0:self.winner_count]:
                team = Team.objects.get(pk=team_id)
                league_winners.append(team)
            winners.append(league_winners)

        match_list = []
        for i in range(0, self.winner_count):
            shuffle(winners)
            for l_winners in winners:
                shuffle(l_winners)
                match_list.append(l_winners[i])

        if len(match_list) % 2:
            match_list.append(match_list[-1])  # append last team to the end so we can create a self played match for by pass

        if len(match_list) == 2:
            self.final_created = True

        first_matches = []
        for i in range(0, len(match_list), 2):
            new_match = matchmodels.Match()
            new_match.home = match_list[i]
            new_match.away = match_list[i+1]
            new_match.week = 1  # week used as round in knockout stage
            new_match.knockout_id = self.pk
            new_match.type = 2
            new_match.save()
            first_matches.append(new_match)

        if self.return_match and not self.final_created:
            match_index = 0
            for i in range(0, len(match_list), 2):
                new_match = matchmodels.Match()
                new_match.home = match_list[i+1]
                new_match.away = match_list[i]
                new_match.week = 1  # week used as round in knockout stage
                new_match.knockout_id = self.pk
                new_match.type = 2
                # new_match.return_match = first_matches[match_index]
                new_match.save()

                first_matches[match_index].return_match = new_match
                match_index += 1

            for match in first_matches:
                match.save(update_fields="return_match")

        if len(match_list) == 2:
            self.final_created = True
        self.knockout_stage = True
        self.tour = 1
        super().save()
        return True

    def check_knockout_tours(self):
        tour_tms = tm_models.TourneyMatches.objects.filter(Q(type=2) & (Q(knockout_id=self.pk) | Q(week=self.tour)))

        tour_winners = []
        for tm in tour_tms:
            match = matchmodels.Match.objects.get(pk=tm.match_id)
            if match.played:
                if self.return_match and not self.final_created:
                    if match.return_match is None:  # if match is a return match just don't care about it
                        continue
                    elif not match.return_match.played:
                        return False

                    if match.get_goal_difference_winner():
                        tour_winners.append(match.get_goal_difference_winner())
                    else:
                        # create one more match to find winner
                        new_match = matchmodels.Match()
                        new_match.home = match.home
                        new_match.away = match.away
                        new_match.week = self.tour  # week used as tour in knockout stage
                        new_match.knockout_id = self.pk
                        new_match.type = 2
                        new_match.save()
                        match.return_match.extra_match  = new_match
                        match.return_match.super_save()
                        return False
                else:
                    tour_winners.append(match.get_winner())
            else:
                return False
        if len(tour_winners) % 2:
            tour_winners.append(tour_winners[-1])  # append last team to the end so we can create a self played match for by pass

        print(tour_winners)
        # if there are only 2 winner this is going to be a final tour
        #################
        if len(tour_winners) == 2:
            self.final_created = True

        # if all matches played then create new tour matches
        self.tour += 1
        super().save()
        first_matches = []
        for i in range(0, len(tour_winners), 2):
            new_match = matchmodels.Match()
            new_match.home = tour_winners[i]
            new_match.away = tour_winners[i+1]
            new_match.week = self.tour  # week used as tour in knockout stage
            new_match.knockout_id = self.pk
            new_match.type = 2
            first_matches.append(new_match)
            new_match.save()

        if self.return_match and not self.final_created:
            match_index = 0
            for i in range(0, len(tour_winners), 2):
                new_match = matchmodels.Match()
                new_match.home = tour_winners[i+1]
                new_match.away = tour_winners[i]
                new_match.week = self.tour  # week used as round in knockout stage
                new_match.knockout_id = self.pk
                new_match.type = 2
                # new_match.return_match = first_matches[match_index]
                new_match.save()

                first_matches[match_index].return_match = new_match
                match_index += 1

            for match in first_matches:
                match.save(update_fields="return_match")

    def set_finished(self, winner):
        self.winner = winner
        self.finished = True
        super().save()


class League(models.Model):
    name = models.CharField(max_length=100)
    knockout = models.ForeignKey(Knockout, null=True)
    team_count = models.IntegerField()
    teams = models.CharField(validators=[validate_comma_separated_integer_list], max_length=400, null=True)
    fixture = models.CharField(max_length=1000, null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        is_update = 0 if self.pk is None else 1
        teams = self.teams.split(',')
        # scoreboard = {int(team_id): 0 for team_id in teams}
        # self.scoreboard = json.dumps(scoreboard)

        super().save()  # to create pk
        if is_update:
            self.update_matches()
        else:
            self.create_matches()
        super().save()  # to save fixture

    def fixture_generate(self, teams_original):  # tc means team_count
        result = []
        # teams = list(range(1, tc+1))
        teams = list(teams_original)
        tc = len(teams)
        if tc % 2:
            teams.append(0)
            tc += 1

        for i in range(0, tc-1):
            result.append([])
            for j in range(0, tc//2):
                # print(a[0+j], a[11-j])
                result[i].append((teams[0+j], teams[tc-1-j]))
            teams.insert(1, teams[tc-1])
            del teams[tc]

        return result

    def delete_matches(self):
        db_fixture = eval(self.fixture)
        for week in db_fixture:
            for match_id in week:
                m = matchmodels.Match.objects.get(pk=match_id)
                m.delete()

    def create_matches(self):
        fixture = self.fixture_generate(self.teams.split(','))

        self.fixture = []
        # for first matches
        first_half_matches = []
        for wi, week in enumerate(fixture):
            self.fixture.append([])
            for match in week:
                if match[0] == 0 or match[1] == 0:
                    continue

                new_match = matchmodels.Match()
                new_match.home_id = match[0]
                new_match.away_id = match[1]
                new_match.league_id = self.pk
                new_match.week = wi + 1
                new_match.group = 0
                new_match.type = 1
                new_match.save()
                first_half_matches.append(new_match)
                self.fixture[wi].append(new_match.pk)
        # for return matches
        match_index = 0  # to keep matched with first half
        for wi, week in enumerate(fixture):
            self.fixture.append([])
            for match in week:
                if match[0] == 0 or match[1] == 0:
                    continue

                new_match = matchmodels.Match()
                new_match.home_id = match[1]
                new_match.away_id = match[0]
                new_match.league_id = self.pk
                new_match.week = wi + 1 + len(fixture)  # to simulate second half of the league
                new_match.group = 0
                new_match.type = 1
                new_match.return_match = first_half_matches[match_index]
                new_match.save()
                first_half_matches[match_index].return_match = new_match
                match_index += 1
                self.fixture[wi+len(fixture)].append(new_match.pk)

        for match in first_half_matches:
            match.save(update_fields="return_match")

    def update_matches(self):
        fixture = self.fixture_generate(self.teams.split(','))

        matches = []
        db_fixture = eval(self.fixture)
        for week in db_fixture:
            for match_id in week:
                matches.append(matchmodels.Match.objects.get(pk=match_id))

        self.fixture = []
        # for first matches
        match_index = 0
        for wi, week in enumerate(fixture):
            self.fixture.append([])
            for match in week:
                if match[0] == 0 or match[1] == 0:
                    continue
                update_match = matches[match_index]
                update_match.home_id = match[0]
                update_match.away_id = match[1]
                update_match.save()
                self.fixture[wi].append(update_match.pk)
                match_index += 1

        # for return matches
        for wi, week in enumerate(fixture):
            self.fixture.append([])
            for match in week:
                if match[0] == 0 or match[1] == 0:
                    continue

                update_match = matches[match_index]
                update_match.home_id = match[1]
                update_match.away_id = match[0]
                update_match.save()
                self.fixture[wi+len(fixture)].append(update_match.pk)
                match_index += 1

    def get_scoreboard(self):
        fixture = eval(self.fixture)
        teams = self.teams.split(',')
        scoreboard = {int(teams[i]): {'pm': 0, 'w': 0, 'l': 0, 'd': 0, 'gf': 0, 'ga': 0, 'pts': 0} for i in range(0, len(teams))}
        goals = {int(teams[i]): 0 for i in range(0, len(teams))}
        matches = []
        matches_dict = {}
        for week in fixture:
            for match_id in week:
                match = matchmodels.Match.objects.get(pk=match_id)
                matches.append(match)
                matches_dict[str(match.home.pk)+str(match.away.pk)] = match

        for match in matches:
            if match.played:
                scoreboard[match.home.pk]['pm'] += 1
                scoreboard[match.home.pk]['gf'] += match.result_home
                scoreboard[match.home.pk]['ga'] += match.result_away

                scoreboard[match.away.pk]['pm'] += 1
                scoreboard[match.away.pk]['gf'] += match.result_away
                scoreboard[match.away.pk]['ga'] += match.result_home

                if match.get_result() == match.HOME_WIN:
                    scoreboard[match.home.pk]['pts'] += 3
                    scoreboard[match.home.pk]['w'] += 1
                    scoreboard[match.away.pk]['l'] += 1
                elif match.get_result() == match.DRAW:
                    scoreboard[match.home.pk]['pts'] += 1
                    scoreboard[match.away.pk]['pts'] += 1
                    scoreboard[match.home.pk]['d'] += 1
                    scoreboard[match.away.pk]['d'] += 1
                elif match.get_result() == match.AWAY_WIN:
                    scoreboard[match.away.pk]['pts'] += 3
                    scoreboard[match.away.pk]['w'] += 1
                    scoreboard[match.home.pk]['l'] += 1

        # scoreboard = sorted(scoreboard.items(), key=operator.itemgetter(1), reverse=True)
        # return sorted(scoreboard.items(), key=lambda x: x[1]['pts'], reverse=True)
        scoreboard = sorted(scoreboard.items(), key=lambda x: x[1]['pts'], reverse=True)
        scoreboard = [list(i) for i in scoreboard]
        # now scoreboard turned into a list that sorted according to points
        # need to check for equal points last time to be sure for sorting is done
        for team_id, row in scoreboard:
            for i in range(0, self.team_count-1):
                if scoreboard[i][1]['pts'] != scoreboard[i+1][1]['pts']:
                    continue
                else:
                    team1 = Team.objects.get(pk=scoreboard[i][0])
                    team2 = Team.objects.get(pk=scoreboard[i+1][0])

                    match1 = matches_dict[str(team1.pk) + str(team2.pk)]  # for first match team1 is home
                    match2 = matches_dict[str(team2.pk) + str(team1.pk)]  # for second match team2 is home

                    if not match1.played or not match2.played:
                        continue
                    elif match2.result_away > match1.result_away:  # means team1 has more away goals
                        #  we don't need to swap the teams in scoreboard
                        pass
                    elif match2.result_away < match1.result_away:  # means team2 has more away goals
                        # swap the teams in scoreboard
                        tmp_row = scoreboard[i][1]
                        scoreboard[i][0] = team2.pk
                        scoreboard[i][1] = scoreboard[i+1][1]
                        scoreboard[i+1][0] = team1.pk
                        scoreboard[i+1][1] = tmp_row
                    else:
                        # check for total goals count
                        if goals[team1.pk] > goals[team2.pk]:
                            # we don't need to swap the teams
                            pass
                        elif goals[team1.pk] < goals[team2.pk]:
                            # swap the teams in scoreboard
                            tmp_row = scoreboard[i][1]
                            scoreboard[i][0] = team2.pk
                            scoreboard[i][1] = scoreboard[i+1][1]
                            scoreboard[i+1][0] = team1.pk
                            scoreboard[i+1][1] = tmp_row
        return scoreboard

    def super_save(self):
        super().save()
