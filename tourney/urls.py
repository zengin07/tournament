from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^create$', views.CreateLeague.as_view(), name='create_league'),
    url(r'^list$', views.ListTourneys.as_view(), name='list_tourney'),
    url(r'^edit/(?P<league_id>[0-9]+)$', views.EditLeague.as_view(), name='edit_league'),
    url(r'^k/create$', views.CreateKnockout.as_view(), name='create_knockout'),
    url(r'^k/edit/(?P<knockout_id>[0-9]+)$', views.EditKnockout.as_view(), name='edit_knockout'),
    url(r'^league/(?P<league_id>[0-9]+)$', views.LeaguePage.as_view(), name='league_page'),
    url(r'^knockout/(?P<knockout_id>[0-9]+)$', views.KnockoutPage.as_view(), name='knockout_page'),
]
