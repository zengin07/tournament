from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^(?P<player_id>[0-9]+)$', views.PlayerPage.as_view(), name='player_page'),
    url(r'^edit$', views.EditProfile.as_view(), name='edit_profile'),
    url(r'^edit/(?P<user_id>[0-9]+)$', views.EditProfile.as_view(), name='edit_profile_id'),
    url(r'^offer$', views.TransferOfferView.as_view(), name='transfer_offer'),
    url(r'^offer/(?P<offer_id>[0-9]+)$', views.TransferOfferAnswer.as_view(), name='offer_answer'),
    url(r'^search$', views.SearchUser.as_view(), name='search_user'),

]
