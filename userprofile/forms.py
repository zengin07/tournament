from django import forms
from django.forms import ModelForm
from registration.forms import RegistrationFormUniqueEmail
from userprofile.choices import *
from django.contrib.auth.models import User


class UserProfileRegisterForm(RegistrationFormUniqueEmail):
    position = forms.ChoiceField(label="Position", initial=1, widget=forms.Select(), required=True, choices=POSITION)
    origin_id = forms.CharField()
    # user_type = forms.ChoiceField(label="User Type", initial=3, widget=forms.Select(), required=True, choices=USER_TYPE)


class UserProfileForm(ModelForm):
    position = forms.ChoiceField(label="Position", initial=1, widget=forms.Select(), choices=POSITION)
    origin_id = forms.CharField()
    # password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].required = False
        self.fields['last_name'].required = False


class UserProfileFormAdmin(UserProfileForm):
    user_type = forms.ChoiceField(label="User Type", initial=3, widget=forms.Select(), choices=USER_TYPE)

    class Meta(UserProfileForm.Meta):
        UserProfileForm.Meta.fields += ['is_active', 'date_joined']
