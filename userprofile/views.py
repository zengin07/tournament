from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views import View
from django.db.models import Q

from tournament.forms import TransferOfferForm
from tournament.models import TransferOffer, TeamCaptains, PlayerTransfers
from .forms import *
from .models import *
from registration.backends.default.views import RegistrationView


# Create your views here.

class MyRegistrationView(RegistrationView):

    form_class = UserProfileRegisterForm

    def register(self, form_class):
        new_user = super(MyRegistrationView, self).register(form_class)
        user_profile = UserProfile()
        user_profile.user = new_user
        user_profile.position = form_class.cleaned_data['position']
        user_profile.origin_id = form_class.cleaned_data['origin_id']
        user_profile.user_type = 3
        user_profile.save()
        return user_profile


class EditProfile(LoginRequiredMixin, View):

    def get(self, request, user_id=0):
        current_user_profile = UserProfile.objects.get(user_id=request.user.pk)
        user_id = int(user_id)

        user_profile = {}
        if user_id == 0:
            if current_user_profile.user_type == ADMIN:
                user_profiles = UserProfile.objects.all()
                return render(request, "userprofile/list_users.html", {'user_profiles': user_profiles})
            else:
                return redirect('edit_profile_id', user_id=request.user.pk)
        elif current_user_profile.user_type == ADMIN or user_id == request.user.pk:
            user_profile = UserProfile.objects.get(user_id=user_id)
        else:
            return render(request, 'tournament/index.html', {'info_message': 'Access Denied'})

        form_variables = user_profile.__dict__
        form_variables.update(user_profile.user.__dict__)
        del form_variables["password"]

        up_form = UserProfileFormAdmin(initial=form_variables) if current_user_profile.user_type == ADMIN else UserProfileForm(initial=form_variables)
        context = {'UserProfileForm': up_form, 'user_profile': user_profile}
        return render(request, 'userprofile/edit_profile.html', context)

    def post(self, request, user_id=0):

        current_user_profile = UserProfile.objects.get(user_id=request.user.pk)
        if user_id == 0:
            return redirect('edit_profile_id', user_id=request.user.pk)
        elif current_user_profile.user_type == ADMIN:
            user_profile = UserProfile.objects.get(user_id=user_id)
        elif current_user_profile.user_id != request.user.pk:
            return render(request, 'tournament/index.html', {'info_message': 'Access Denied!'})
        else:
            user_profile = UserProfile.objects.get(user_id=request.user.pk)

        form = UserProfileFormAdmin(request.POST, instance=user_profile.user) if current_user_profile.user_type == ADMIN else UserProfileForm(request.POST, instance=user_profile.user)
        if form.is_valid():
            user_profile.user.username = form.cleaned_data['username']
            user_profile.user.first_name = form.cleaned_data['first_name']
            user_profile.user.last_name = form.cleaned_data['last_name']
            user_profile.user.email = form.cleaned_data['email']
            user_profile.position = form.cleaned_data['position']
            user_profile.origin_id = form.cleaned_data['origin_id']
            if current_user_profile.user_type == ADMIN:
                user_profile.user_type = form.cleaned_data['user_type']
                user_profile.user.is_active = form.cleaned_data['is_active']
                user_profile.user.date_joined = form.cleaned_data['date_joined']
            user_profile.save()
            user_profile.user.save()

            return redirect('edit_profile_id', user_id=user_id)
        else:
            return render(request, 'userprofile/edit_profile.html', {'UserProfileForm': form})


class PlayerPage(View):
    def get(self, request, player_id, info_message=''):
        if request.user.is_authenticated():
            cup = UserProfile.objects.get(user_id=request.user.pk) # cup=current_user_profile
        else:
            cup = {}
        cansee_offers = True if int(player_id) == request.user.pk else False
        offers = TransferOffer.objects.filter(receiver_id=request.user.pk, is_answered=False)
        player_transfers = PlayerTransfers.objects.filter(player_id=cup.user_id)
        context = {
            'offers': offers,
            'cansee_offers': cansee_offers,
            'cup': cup,
            'user_profile': UserProfile.objects.get(user_id=player_id),
            'player_transfers': player_transfers,
        }
        if info_message:
            context['info_message'] = info_message
        return render(request, "userprofile/player_page.html", context)


class TransferOfferView(LoginRequiredMixin, View):
    def get(self, request):
        current_user_profile = UserProfile.objects.get(user_id=request.user.pk)
        if current_user_profile.user_type != ADMIN and current_user_profile.user_type != CAPTAIN:
            return render(request, 'tournament/index.html', {'info_message': 'Access Denied'})

        form = TransferOfferForm(for_admin=True) if current_user_profile.user_type == ADMIN else TransferOfferForm()
        context = {
            'form': form
        }
        return render(request, "userprofile/transfer_offer.html", context)

    def post(self, request):
        current_user_profile = UserProfile.objects.get(user_id=request.user.pk)
        if current_user_profile.user_type != ADMIN and current_user_profile.user_type != CAPTAIN:
            return render(request, 'tournament/index.html', {'info_message': 'Access Denied'})

        form = TransferOfferForm(request.POST, for_admin=True) if current_user_profile.user_type == ADMIN else TransferOfferForm(request.POST, )
        if form.is_valid():
            if current_user_profile.user_type == ADMIN:
                sender_team = form.cleaned_data['sender_team']
            elif current_user_profile.user_type == CAPTAIN:
                sender_team = current_user_profile.team
            receiver = form.cleaned_data['receiver']
            to = TransferOffer(sender_team=sender_team, receiver=receiver)
            to.save()

            return render(request, "userprofile/transfer_offer.html", {'form': form, 'info_message': 'Offer sent'})
        else:
            return render(request, "userprofile/transfer_offer.html", {'form': form})


class TransferOfferAnswer(LoginRequiredMixin, View):
    def get(self, request, offer_id):
        current_user_profile = UserProfile.objects.get(user_id=request.user.pk)
        offer = TransferOffer.objects.get(pk=offer_id)

        if offer.receiver == current_user_profile:
            offer.is_answered = True
            if int(request.GET['accepted']):
                current_user_profile.team_id = offer.sender_team_id
                if current_user_profile.user_type != ADMIN:
                    current_user_profile.user_type = PLAYER
                current_user_profile.save()

                if current_user_profile.user_type == CAPTAIN:
                    tc = TeamCaptains.objects.get(player_id=current_user_profile.user_id)
                    tc.delete()
                    tc.save()

                transfer = PlayerTransfers()
                transfer.player = current_user_profile
                transfer.team = offer.sender_team
                transfer.save()

                offer.is_accepted = True
                offer.delete()
                return PlayerPage().get(request=request, player_id=request.user.pk, info_message='Offer Accepted')
            else:
                offer.is_accepted = False
                offer.delete()
                return PlayerPage().get(request=request, player_id=request.user.pk, info_message='Offer Rejected')
        else:
            return render('tournament/index.html', info_message='Access Denied!')


class SearchUser(View):
    def get(self, request):
        q = request.GET['q']
        user_results = User.objects.filter(Q(username__icontains=q))

        user_profile_results = []
        for user in user_results:
            print(user.username)
            user_profile_results.append(UserProfile.objects.get(user_id=user.pk))

        context = {'user_profiles': user_profile_results}

        return render(request, 'userprofile/list_users.html', context)
