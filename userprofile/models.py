from django.db import models
from django.contrib.auth.models import User
from userprofile.choices import *
from team.models import Team


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    position = models.IntegerField(choices=POSITION, default=0)
    origin_id = models.CharField(max_length=30)
    user_type = models.IntegerField(choices=USER_TYPE, default=3)
    team = models.ForeignKey(Team, null=True)

    def __str__(self):
        return self.origin_id
