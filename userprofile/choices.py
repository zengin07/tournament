ADMIN = 1
CAPTAIN = 2
PLAYER = 3
USER_TYPE = (
    (ADMIN, "Admin"),
    (CAPTAIN, "Captain"),
    (PLAYER, "Player")
)

GK = 1
LB = 2
LWB = 3
CB = 4
RB = 5
RWB = 6
LM = 7
CM = 8
CDM = 9
CAM = 10
RM = 11
LW = 12
RW = 13
LF = 14
CF = 15
RF = 16
ST = 17
POSITION = (
    (GK, "GK- Goalkeeper"),
    (LB, "LB- Left Back"),
    (LWB, "LWB- Left Wing Back"),
    (CB, "CB- Centre Back"),
    (RB, "RB- Right Back"),
    (RWB, "RWB- Right Wing Back"),
    (LM, "LM- Left Midfielder"),
    (CM, "CM- Centre Midfielder"),
    (CDM, "CDM- Centre Defensive Midfielder"),
    (CAM, "CAM- Centre Attacking Midfielder"),
    (RM, "RM- Right Midfielder"),
    (LW, "LW- Left Winger"),
    (RW, "RW- Right Winger"),
    (LF, "LF- Left Forward"),
    (CF, "CF- Centre Forward"),
    (RF, "RF- Right Forward"),
    (ST, "ST- Striker")
)



















